import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import CustomizedInputBase from './HomePage/SearchBar';
import SwipeableTemporaryDrawer from './HambMenu';
import {Link} from 'react-router-dom';

const styles = theme => ({
    btn:{
        width: "100%",
        },
        root: {
          flexGrow: 1,
              justifyContent: 'center',
              width: '100%',
         
              backgroundColor: theme.palette.background.paper,
        },
        
        grow: {
          flexGrow: 1,
        },
       
        root: {
          display: 'flex',
          flexWrap: 'wrap',
          minWidth: 300,
          width: '100%',
        },
        
       
       
    });



function MainHeader(props) {
    const { classes } = props;
    
  
    return (
      <div className={classes.root}>
        <Grid container spacing={8}>
          <Grid item xs={12}>
          <div className={classes.root}>
        <AppBar color="secondary" position="static">
          <Toolbar>
          <SwipeableTemporaryDrawer/>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              CCSI Logo
            </Typography>
           
            <CustomizedInputBase/>
           <Button color="inherit">Login</Button>
            <Button color="inherit">Sign up</Button>
          </Toolbar>
        </AppBar>
      </div>
          </Grid>
          <Grid item xs={3}> 
          
       <Link to="/"><Button variant="contained"  size="large" color="primary" className={classes.btn}>
          Home
        </Button>
        </Link>
        
        </Grid>
        <Grid item xs={3}>
        
        <Link to="/blogs"><Button variant="contained" size="large" color="primary" className={classes.btn}>
          Blogs
        </Button>
        </Link>
        </Grid>
        <Grid item xs={3}>
          <Button variant="contained" size="large" color="primary" className={classes.btn}>
          Challenges
        </Button>
        </Grid>
        <Grid item xs={3}>
          <Button variant="contained" size="large" color="primary" className={classes.btn}>
          About
        </Button>
          </Grid>
          <Grid item xs={12}>
          <Divider/>
          </Grid>
          </Grid>
          </div>
    )
};
MainHeader.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(MainHeader);
  
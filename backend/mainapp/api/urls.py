from django.urls import path
from . import views 

from .views import  BlogsListView, BlogsDetailView, KategoriteListView, KategoriteDetailView, ChallengesListView, ChallengesDetailView, Comments_challengesListView, Comments_challengesDetailView, Comments_blogsListView, Comments_blogsDetailView


urlpatterns = [
    path('blogs/', BlogsListView.as_view()),
    path('blogs/<pk>', BlogsDetailView.as_view()),

     path('kategorite/', KategoriteListView.as_view()),
     path('kategorite/<pk>', KategoriteDetailView.as_view()),

     path('challenges/', ChallengesListView.as_view()),
     path('challenges/<pk>', ChallengesDetailView.as_view()),

     path('commentschallenges/', Comments_challengesListView.as_view()),
     path('commentschallenges/<pk>', Comments_challengesDetailView.as_view()),

     path('commentsblogs/', Comments_blogsListView.as_view()),
     path('commentsblogs/<pk>', Comments_blogsDetailView.as_view()),

    path('register/', views.register, name='register'),

    path('accounts/login/', views.login, name='login'),
    path('accounts/logout/', views.logout, name='logout'),
]
